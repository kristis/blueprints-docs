.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ./definitions.rst

.. _BlueprintsFAQ:

FAQ
###

Why does my build fail with random error?
*****************************************

If the `bitbake` (build) command fails, most of the time it is due to an
outdated set of sources. Make sure you update both your `repo` (`repo sync`)
workspace and the all the other layers detailed in the :ref:`blueprints
workspace documentation <Workspace>`.

If updating the sources doesn't fix the issue in an existing build, create a
new build environment and retry. `sstate` and `downloads` can be shared or
migrated from the old build if wanted.

My build fails with `DISTRO 'YYY' not found.`. What do I do?
************************************************************

Builds require the path of the `meta-oniro-blueprints` checkout in
`bblayers.conf`. This means that when a build is initialized, this layer needs
to be included in the build's configuration:

.. code-block:: bash

   bitbake-layers add-layer $CHECKOUT_DIR/meta-oniro-blueprints

The general rule is, once you create a new blueprint build directory, make sure
you have cloned/updated and included all the layers mentioned in the
:ref:`blueprints workspace documentation <Workspace>`.

My build fails with `ERROR: Nothing PROVIDES 'YYY'`. What do I do?
******************************************************************

This most probably has the same roots as the one above: missed adding the
`meta-oniro-blueprints` layer path to build's configuration. Make sure you have
run the `bitbake-layers add-layer` commands advertised in the :ref:`blueprints
workspace documentation <Workspace>`.
